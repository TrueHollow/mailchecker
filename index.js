require('./services/dotEnv_proxy');
const config = require('./config');
const Browser = require('./services/Browser');
const YahooChecker = require('./services/YahooChecker');
const logger = require('./logger')('index.js');

logger.info('Script is started');

let browser;
const main = async () => {
  browser = new Browser(config.chrome);
  await browser.init();
  const yahoo = new YahooChecker(browser);
  await yahoo.run();
};

main().then(async () => {
  logger.info('Script is finished');
  if (browser) {
    await browser.finish();
  }
});
