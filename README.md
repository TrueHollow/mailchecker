# Email checker

## System requirements:

Node.js version 10 or higher. 

## Prepare

`npm i --production`

## Run

`npm start`

## Support environment files with [`dotenv`](https://www.npmjs.com/package/dotenv#rules)

You can use a `.env` file to set environment variables:
```dotenv
YAHOO_LOGIN=yahoo_login
YAHOO_PASSWORD=yahoo_password
```
