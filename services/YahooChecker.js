const logger = require('../logger')('services/YahooChecker.js');
const {
  CSS_SELECTOR_TIMEOUT,
  createInputDelay,
  delay,
} = require('./ParserCommon');

const LOGIN_URL = 'https://mail.yahoo.com/';
const SIGN_IN_SELECTOR = 'a[data-ylk="mKey:signin_click"]';
const LOGIN_INPUT_SELECTOR = '#login-username';
const PASSWORD_INPUT_SELECTOR = '#login-passwd';

const INBOX_SELECTOR = 'div[data-test-folder-container="Inbox"]';
const SPAM_SELECTOR = 'div[data-test-folder-container="Bulk"]';
const MAIL_SELECTOR =
  'a[data-test-id="message-list-item"][data-test-read="false"]';
const TITLE_SELECTOR = 'span[data-test-id="message-group-subject-text"]';
const RESTORE_SELECTOR = 'button[data-test-id="toolbar-archive-restore"]';
const TOOLBAR_MORE_SELECTOR = 'button[data-test-id="toolbar-more"]';
const UNREAD_SELECTOR = 'li[name="mark-as-unread"]';
const URLS_SELECTOR = 'div[data-test-id="message-view-body-content"] a';

const { YAHOO_LOGIN, YAHOO_PASSWORD } = process.env;

class YahooChecker {
  /**
   * Constructor
   * @param {Browser} browser instance
   */
  constructor(browser) {
    this.browser = browser;
  }

  async login() {
    logger.info('Logging');
    let page;
    try {
      page = await this.browser.getNewPageAndGo(LOGIN_URL);
      await page.waitForSelector(SIGN_IN_SELECTOR, {
        timeout: CSS_SELECTOR_TIMEOUT,
      });
      await Promise.all([
        page.waitForNavigation(),
        page.click(SIGN_IN_SELECTOR, createInputDelay()),
      ]);
      await page.waitForSelector(LOGIN_INPUT_SELECTOR, {
        timeout: CSS_SELECTOR_TIMEOUT,
      });
      await page.click(LOGIN_INPUT_SELECTOR, createInputDelay());
      await page.keyboard.type(YAHOO_LOGIN, createInputDelay());
      await Promise.all([
        page.waitForNavigation(),
        page.keyboard.press('Enter', createInputDelay()),
      ]);
      await page.waitForSelector(PASSWORD_INPUT_SELECTOR, {
        timeout: CSS_SELECTOR_TIMEOUT,
      });
      await page.keyboard.type(YAHOO_PASSWORD, createInputDelay());
      await Promise.all([
        page.waitForNavigation(),
        page.keyboard.press('Enter', createInputDelay()),
      ]);
    } catch (e) {
      logger.error(e);
    } finally {
      if (page) {
        await page.close();
      }
    }
  }

  static async existOnPage(page, selector) {
    return page.evaluate(s => {
      const el = document.querySelector(s);
      return !!el;
    }, selector);
  }

  /**
   * Get text item
   * @param page
   * @param selector
   * @returns {Promise<string|null>}
   */
  static async getText(page, selector) {
    return page.evaluate(s => {
      const el = document.querySelector(s);
      if (!el) {
        console.error(`Item with selector ${s} not found`);
        return null;
      }
      return el.textContent.trim();
    }, selector);
  }

  /**
   * Get urls in mail body.
   * @param page
   * @param selector
   * @returns {Promise<Array<string>>}
   */
  static async getUrlList(page, selector) {
    return page.evaluate(s => {
      const urls = [];
      const aTags = document.querySelectorAll(
        s
      );
      aTags.forEach(aTag => {
        const attr = aTag.getAttribute('href');
        if (attr) {
          urls.push(attr);
        }
      });
      return urls;
    }, selector);
  }

  async parseMails() {
    logger.info('Logging');
    let page;
    try {
      page = await this.browser.getNewPageAndGo(LOGIN_URL);
      await page.waitForSelector(SPAM_SELECTOR, {
        timeout: CSS_SELECTOR_TIMEOUT,
      });
      await page.click(SPAM_SELECTOR, createInputDelay());
      while (await YahooChecker.existOnPage(page, MAIL_SELECTOR)) {
        await page.click(MAIL_SELECTOR, createInputDelay());
        await delay(500);
        const title = await YahooChecker.getText(page, TITLE_SELECTOR);
        // todo is Valid
        if (title.indexOf('testCase') !== -1) {
          await page.click(TOOLBAR_MORE_SELECTOR, createInputDelay());
          await page.click(UNREAD_SELECTOR, createInputDelay());
          await page.click(RESTORE_SELECTOR, createInputDelay());
        } else {
          await page.keyboard.press('Escape', createInputDelay());
        }
        await delay(500);
      }
      await page.click(INBOX_SELECTOR, createInputDelay());
      debugger;
      while (await YahooChecker.existOnPage(page, MAIL_SELECTOR)) {
        await page.click(MAIL_SELECTOR, createInputDelay());
        await delay(500);
        const title = await YahooChecker.getText(page, TITLE_SELECTOR);
        // todo is Valid
        if (title.indexOf('testCase') !== -1) {
          const urls = await YahooChecker.getUrlList(page, URLS_SELECTOR);
          for (const url of urls) {
            const p = await this.browser.getNewPageAndGo(url, 3000);
            await p.close();
          }
        } else {
          await page.keyboard.press('Escape', createInputDelay());
        }
        await delay(500);
      }
      debugger;
    } catch (e) {
      logger.error(e);
    } finally {
      if (page) {
        await page.close();
      }
    }
  }

  async run() {
    logger.info('Run invoked');
    await this.login();
    await this.parseMails();
  }
}

module.exports = YahooChecker;
