const puppeteer = require('puppeteer-extra');
const pluginStealth = require('puppeteer-extra-plugin-stealth');
const { GOTO_OPTIONS, delay } = require('./ParserCommon');

puppeteer.use(pluginStealth());

const logger = require('../logger')('service/Browser.js');

class Browser {
  constructor(config) {
    this.config = config;
    this.browser = null;
  }

  async init() {
    if (this.browser == null) {
      logger.info('Initialize browser instance.');
      this.browser = await puppeteer.launch(this.config);
    }
  }

  async getNewPage() {
    const page = await this.browser.newPage();
    page.on('console', msg =>
      logger.debug('PAGE LOG:', msg.type(), msg.text())
    );
    await page.setViewport({ width: 1920, height: 1040 });
    return page;
  }

  async getNewPageAndGo(url, delayValue = 1000) {
    const page = await this.getNewPage();
    await page.goto(url, GOTO_OPTIONS);
    await delay(delayValue);
    return page;
  }

  async finish() {
    if (this.browser) {
      logger.info('Closing browser.');
      await this.browser.close();
    }
  }
}

module.exports = Browser;
